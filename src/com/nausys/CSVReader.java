package com.nausys;
import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CSVReader {

    private List<DateRange> dateRanges = new ArrayList<DateRange>();
    private List<Plovilo> plovilos = new ArrayList<Plovilo>();

    CSVReader(String inputFilePath){
        this.dateRanges = processFirstLine(inputFilePath);
        this.plovilos = processInputFile(inputFilePath);
    }

    public List<Plovilo> processInputFile(String inputFilePath) {
        List<Plovilo> inputList = new ArrayList<Plovilo>();
        try{
            File inputF = new File(inputFilePath);
            InputStream inputFS = new FileInputStream(inputF);
            BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));
            // skip the header of the csv
            inputList = br.lines().skip(1).map(mapToItem).collect(Collectors.toList());
            br.close();
        } catch (IOException e) {
            System.out.println("File not found!");
        }
        return inputList ;
    }

    private Function<String, Plovilo> mapToItem = (line) -> {
        int i;
        HashMap<DateRange,Integer> cjenik = new HashMap<>();
        String[] p = line.split(";");// CSV has semicolon separated lines
        Plovilo item = new Plovilo();
        item.setID_plovila(Integer.valueOf(p[0]));//<-- this is the first column in the csv file
        item.setNazivPlovila(p[1]);
        item.setGodiste(Short.valueOf(p[2]));
        for(i = 0; i< this.dateRanges.size(); i++){
            cjenik.put(dateRanges.get(i),Integer.valueOf(p[3+i])); //there are as many prices as there are dateranges, prices start from the 3. column
        }
        item.setCjenik(cjenik);
        return item;
    };

    public List<DateRange> processFirstLine(String inputFilePath){
        List<DateRange> dateRangeList = new ArrayList<DateRange>();
        try{
            int i;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            File inputF = new File(inputFilePath);
            InputStream inputFS = new FileInputStream(inputF);
            BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));
            String line,columns[];
            line = br.lines().findFirst().get();
            columns = line.split(";");
            for(i = 3; i < columns.length; i++){
               try {
                   DateRange dr = new DateRange();
                   String dates[];
                   dates = columns[i].split("-"); //dates are separated by a score
                   dr.setFrom(LocalDate.parse(dates[0], formatter));
                   dr.setTo(LocalDate.parse(dates[1], formatter));
                   dateRangeList.add(dr);
               }
               catch (Exception e){
                   System.out.println("The input data isn't properly formatted!");
               }
            }
            br.close();
        } catch (IOException e) {
            System.out.println("File not found!");
        }
        return dateRangeList ;
    }

    public List<Plovilo> getPlovilos() {
       return plovilos;
    }

    public List<DateRange> getDateRanges() {
        return dateRanges;
    }
}
