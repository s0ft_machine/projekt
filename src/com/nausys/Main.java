package com.nausys;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.List;


public class Main {

    public static void main(String[] args) {
        if(args.length < 3 || args.length > 4){
            System.out.println("This program requires at least 3 and allows no more than 4 arguments to run.\n" +
                               "Please enter the ID of the vessel, the date of departure and the number of days" +
                               " the vessel is to be rented.");
            System.out.println("Example usage: java -jar projekt.jar 2 14.04.2019 5");
            System.out.println("Example usage: java -jar projekt.jar 2 14.04.2019 5 path/to/file.csv");
            return;
        }

        int i,j;
        float cijenaNajma;
        List<Plovilo> popis_plovila;
        List<DateRange> popis_datuma;
        String CSVPath = "cjenik.csv";
        int idPlovila, danaIznajmljeno;
        LocalDate datumPolaska,datumIstekaNajma;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        if(args.length == 4){
            CSVPath = args[3];
        }

        idPlovila = Integer.valueOf(args[0]);
        danaIznajmljeno = Integer.valueOf(args[2]);
        datumPolaska = LocalDate.parse(args[1],formatter);
        datumIstekaNajma = datumPolaska.plusDays(danaIznajmljeno);

        CSVReader csv = new CSVReader(CSVPath);
        popis_datuma = csv.getDateRanges();
        popis_plovila = csv.getPlovilos();

        //first, we need to find within which DateRange is datumPolaska
        for(i = 0; i < popis_datuma.size();i++){
            if(popis_datuma.get(i).dateWithinRange(datumPolaska))
                break;
        }

        //after that, we need to find within which DateRange is datumIstekaNajma
        for(j = 0; j < popis_datuma.size(); j++){
            if(popis_datuma.get(j).dateWithinRange(datumIstekaNajma))
                break;
        }

        //if datumPolaska is within the same DateRange as the datumIstekaNajma, the price is constant,
        //so it's easy to calculate the price
        if(i == j) {
            cijenaNajma =  (float)danaIznajmljeno * ((float)1 / 7) * (float)popis_plovila.get(idPlovila-1).getCjenik().get(popis_datuma.get(i));
        }

        else if(j > i) {
            //this part calculates the price from datumPolaska to the end of the DateRange where datumPolaska is located
            Period period = Period.between(datumPolaska, popis_datuma.get(i).getTo());
            cijenaNajma = (period.getDays()+1) * ((float)1 / 7) * popis_plovila.get(idPlovila-1).getCjenik().get(popis_datuma.get(i));
            i++; // this part calculates the price for the periods between the first and the last one. we increment the i variable so we don't
            while(i<j){
                Period srednjiPeriod = Period.between(popis_datuma.get(i).getFrom(), popis_datuma.get(i).getTo());
                cijenaNajma += srednjiPeriod.getDays() * ((float)1 / 7) * popis_plovila.get(idPlovila-1).getCjenik().get(popis_datuma.get(i));
                System.out.println("srednji  " + srednjiPeriod.getDays());
                i++;
            }
            period = Period.between(popis_datuma.get(j).getFrom(),datumIstekaNajma);
            cijenaNajma += period.getDays() * ((float)1/7) * popis_plovila.get(idPlovila-1).getCjenik().get(popis_datuma.get(i));
        }

        else{
            System.out.println("It's impossible to rent a vessel into the past unless your name is Marty McFly. Is it? Smartass...");
            return;
        }

        System.out.println(cijenaNajma);
    }
}
