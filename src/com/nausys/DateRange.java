package com.nausys;

import java.time.LocalDate;

public class DateRange {
    LocalDate from, to;

    public LocalDate getFrom() {
        return from;
    }

    public void setFrom(LocalDate from) {
        this.from = from;
    }

    public LocalDate getTo() {
        return to;
    }

    public void setTo(LocalDate to) {
        this.to = to;
    }

    public boolean dateWithinRange(LocalDate date){
        int comparison1, comparison2;
        comparison1 = from.compareTo(date);
        comparison2 = to.compareTo(date);
        //is the date between or equal to this.from and this.to
        if(comparison1 <= 0 && comparison2 >= 0){
            return true;
        }
        return false;
    }
}
