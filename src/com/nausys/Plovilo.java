package com.nausys;

import java.util.HashMap;

public class Plovilo {
    private int ID_plovila;
    private String nazivPlovila;
    private short godiste;
    HashMap<DateRange,Integer> cjenik;


    public int getID_plovila() {
        return ID_plovila;
    }

    public void setID_plovila(int ID_plovila) {
        this.ID_plovila = ID_plovila;
    }

    public String getNazivPlovila() {
        return nazivPlovila;
    }

    public void setNazivPlovila(String nazivPlovila) {
        this.nazivPlovila = nazivPlovila;
    }

    public short getGodiste() {
        return godiste;
    }

    public void setGodiste(short godiste) {
        this.godiste = godiste;
    }

    public HashMap<DateRange, Integer> getCjenik() {
        return cjenik;
    }

    public void setCjenik(HashMap<DateRange, Integer> cjenik) {
        this.cjenik = cjenik;
    }
}
